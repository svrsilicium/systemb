# System B

The system B implements next methods:

* ***reserve*** product
> It is a POST method that must be sent to URI ```http://localhost:8081/car``` with its **id** as a parameter in the request body.
* ***confirm*** product
> It is a PUT method that must be sent to URI ```http://localhost:8081/car``` with a **hash** of the reservation in the request body.
* ***refuse*** product
> It is a DELETE method thath must be sent to URI ```http://localhost:8081/car``` with a **hash** of the reservation in the request body.

To test system B you can use [Postman](https://www.getpostman.com/) or cURL. 

bash cURL:
```
curl --request POST --url http://localhost:8081/car --form id=2

curl --request PUT --url http://localhost:8081/car --form hash=4961988fe350f728f543e6fe74a34f

curl --request DELETE --url http://localhost:8081/car --form hash=4961988fe350f728f543e6fe74a34f
```