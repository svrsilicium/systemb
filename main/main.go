package main

import (
	"log"
	"net/http"
	"bitbucket.org/svrsilicium/systemB/models"
)

func main() {

	router := models.NewRouter()

	log.Println("listening systemB on 8081")

	log.Fatal(http.ListenAndServe(":8081", router))
}