package models

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"Index",
		"GET",
		"/",
		Index,
	},
	Route{
		"CarReserve",
		"POST",
		"/car",
		CarReserve,
	},
	Route{
		"CarConfirm",
		"PUT",
		"/car",
		CarConfirm,
	},
	Route{
		"CarRefuse",
		"DELETE",
		"/car",
		CarRefuse,
	},
}