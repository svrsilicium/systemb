package models

import (
	"net/http"
	"fmt"
	"io/ioutil"
	"log"
	"bytes"
	"encoding/json"
	"net/http/httputil"
)

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Welcome to systemB!")
}

func CarReserve(w http.ResponseWriter, r *http.Request) {
	/*
	Implements GET method to reserve product
	However, it is better to make POST query
	and for GET to show info about product or reservation
	Will be implemented later, for now it is to show work with
	form-data, however, it is said to be legacy option
	 */
	carId := r.PostFormValue("id")

	//fmt.Printf("*** type=%T\tvar=%s\n", carId, carId)

	resp, err := http.Get("http://127.0.0.1:8080/car/" + carId)
	if err != nil {
		fmt.Println("some error on GET to 8080/car/{carId}")
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("some error reading body")
	}
	fmt.Printf("reservation hash: %s\n", body)
	fmt.Fprintln(w, "Your order's hash is: ", string(body))
}

func CarConfirm(w http.ResponseWriter, r *http.Request) {
	/*
	Implements PUT method for reservation confirmation
	 */
	fmt.Println("*** CarConfirm")

	hash := r.PostFormValue("hash")

	fmt.Printf("*** came: [%T]\t[%s]\n", hash, hash)

	postUrl := "http://127.0.0.1:8080/car"

	client := &http.Client{}
	hashBody := HashBody{Hash: hash}
	fmt.Println(hashBody)
	bHash := new(bytes.Buffer)
	json.NewEncoder(bHash).Encode(hashBody)

	// -- sending hash as json to System A
	req, err := http.NewRequest(http.MethodPut, postUrl, bHash)
	if err != nil {
		fmt.Println("some error on Put to 8080/car")
	}
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")

	// -- printing request, it is not needed, but looks good
	requestDump, err1 := httputil.DumpRequest(req, true)

	if err1 != nil {
		fmt.Println(err1)
	}
	fmt.Println(string(requestDump))

	// -- sending request, receiving response
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	} else {
		defer resp.Body.Close()
		contents, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("The calculated length is:", len(string(contents)), "for the url:", postUrl)
		fmt.Println("   ", resp.StatusCode)
		hdr := resp.Header
		for key, value := range hdr {
			fmt.Println("   ", key, ":", value)
		}
		fmt.Println(string(contents))
		fmt.Fprintln(w, string(contents))
	}
}

func CarRefuse(w http.ResponseWriter, r *http.Request) {
	/*
	DELETE method, to be implemented later
	 */
	fmt.Println("CarRefuse")

	hash := r.PostFormValue("hash")

	fmt.Printf("*** type=%T\tvar=%s\n", hash, hash)
}