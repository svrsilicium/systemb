package models

import "time"

type Car struct {
	Id        int       `json:"id"`
	Name      string    `json:"name"`
	Reserved  bool      `json:"reserved"`
	Due       time.Time `json:"due"`
}

type Cars []*Car